SANE Standard
=============

.. toctree::
   :numbered: 4
   :maxdepth: 3
   :caption: Contents:

   preface
   introduction
   environment
   api
   net
   contact

.. only:: html

   :ref:`genindex`
   ---------------
